// Shader created with Shader Forge v1.32 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.32;sub:START;pass:START;ps:flbk:Shader Forge/shander_hole,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4013,x:32719,y:32712,varname:node_4013,prsc:2|spec-1572-OUT,emission-9558-OUT;n:type:ShaderForge.SFN_TexCoord,id:7413,x:30836,y:32731,varname:node_7413,prsc:2,uv:0;n:type:ShaderForge.SFN_Tex2d,id:8063,x:31909,y:32891,ptovrint:False,ptlb:node_8063,ptin:_node_8063,varname:_node_8063,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:02ed7ac89302433418c895bea56a58f1,ntxv:0,isnm:False|UVIN-229-UVOUT;n:type:ShaderForge.SFN_Rotator,id:526,x:31357,y:32659,varname:node_526,prsc:2|UVIN-7413-UVOUT,SPD-2869-OUT;n:type:ShaderForge.SFN_Panner,id:229,x:31625,y:32888,varname:node_229,prsc:2,spu:0,spv:0|UVIN-526-UVOUT,DIST-2869-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2869,x:30831,y:32912,ptovrint:False,ptlb:node_2869,ptin:_node_2869,varname:_node_2869,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_TexCoord,id:1760,x:30728,y:33303,varname:node_1760,prsc:2,uv:0;n:type:ShaderForge.SFN_Tex2d,id:9690,x:31236,y:33265,ptovrint:False,ptlb:node_9690,ptin:_node_9690,varname:_node_9690,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False|UVIN-855-UVOUT;n:type:ShaderForge.SFN_Multiply,id:8896,x:31510,y:33260,varname:node_8896,prsc:2|A-2896-OUT,B-9690-RGB,C-2169-OUT;n:type:ShaderForge.SFN_OneMinus,id:7485,x:30840,y:33048,varname:node_7485,prsc:2|IN-8063-B;n:type:ShaderForge.SFN_ValueProperty,id:8597,x:30675,y:33194,ptovrint:False,ptlb:node_8597,ptin:_node_8597,varname:_node_8597,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:14;n:type:ShaderForge.SFN_Power,id:2896,x:31061,y:33047,varname:node_2896,prsc:2|VAL-7485-OUT,EXP-8597-OUT;n:type:ShaderForge.SFN_Color,id:7858,x:31483,y:33069,ptovrint:False,ptlb:node_7858,ptin:_node_7858,varname:_node_7858,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1813365,c2:0.2964234,c3:0.6323529,c4:1;n:type:ShaderForge.SFN_Multiply,id:3823,x:31705,y:33264,varname:node_3823,prsc:2|A-7858-RGB,B-8896-OUT;n:type:ShaderForge.SFN_Parallax,id:855,x:31002,y:33321,varname:node_855,prsc:2|UVIN-1760-UVOUT,HEI-1760-U,REF-8597-OUT;n:type:ShaderForge.SFN_Slider,id:1572,x:32330,y:32506,ptovrint:False,ptlb:node_1572,ptin:_node_1572,varname:_node_1572,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.9825706,max:1;n:type:ShaderForge.SFN_Time,id:7160,x:31001,y:33746,varname:node_7160,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:397,x:31005,y:33951,ptovrint:False,ptlb:node_397,ptin:_node_397,varname:_node_397,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Divide,id:1839,x:31237,y:33755,varname:node_1839,prsc:2|A-7160-T,B-397-OUT;n:type:ShaderForge.SFN_Cos,id:7242,x:31426,y:33746,varname:node_7242,prsc:2|IN-1839-OUT;n:type:ShaderForge.SFN_Multiply,id:2169,x:31635,y:33734,varname:node_2169,prsc:2|A-8911-RGB,B-7242-OUT,C-397-OUT,D-8575-OUT,E-7858-B;n:type:ShaderForge.SFN_Color,id:8911,x:31262,y:33562,ptovrint:False,ptlb:node_8911,ptin:_node_8911,varname:_node_8911,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.04685583,c3:0.9705882,c4:1;n:type:ShaderForge.SFN_Slider,id:8575,x:31443,y:33480,ptovrint:False,ptlb:node_8575,ptin:_node_8575,varname:_node_8575,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:18.19218,max:50;n:type:ShaderForge.SFN_Add,id:9558,x:32264,y:32995,varname:node_9558,prsc:2|A-8063-RGB,B-3823-OUT;proporder:2869-8063-9690-8597-7858-1572-397-8911-8575;pass:END;sub:END;*/

Shader "Shader Forge/shander_hole_2" {
    Properties {
        _node_2869 ("node_2869", Float ) = 0.1
        _node_8063 ("node_8063", 2D) = "white" {}
        _node_9690 ("node_9690", 2D) = "white" {}
        _node_8597 ("node_8597", Float ) = 14
        _node_7858 ("node_7858", Color) = (0.1813365,0.2964234,0.6323529,1)
        _node_1572 ("node_1572", Range(0, 1)) = 0.9825706
        _node_397 ("node_397", Float ) = 2
        _node_8911 ("node_8911", Color) = (0,0.04685583,0.9705882,1)
        _node_8575 ("node_8575", Range(0, 50)) = 18.19218
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform sampler2D _node_8063; uniform float4 _node_8063_ST;
            uniform float _node_2869;
            uniform sampler2D _node_9690; uniform float4 _node_9690_ST;
            uniform float _node_8597;
            uniform float4 _node_7858;
            uniform float _node_1572;
            uniform float _node_397;
            uniform float4 _node_8911;
            uniform float _node_8575;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 0.5;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float3 specularColor = float3(_node_1572,_node_1572,_node_1572);
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
////// Emissive:
                float4 node_5549 = _Time + _TimeEditor;
                float node_526_ang = node_5549.g;
                float node_526_spd = _node_2869;
                float node_526_cos = cos(node_526_spd*node_526_ang);
                float node_526_sin = sin(node_526_spd*node_526_ang);
                float2 node_526_piv = float2(0.5,0.5);
                float2 node_526 = (mul(i.uv0-node_526_piv,float2x2( node_526_cos, -node_526_sin, node_526_sin, node_526_cos))+node_526_piv);
                float2 node_229 = (node_526+_node_2869*float2(0,0));
                float4 _node_8063_var = tex2D(_node_8063,TRANSFORM_TEX(node_229, _node_8063));
                float2 node_855 = (0.05*(i.uv0.r - _node_8597)*mul(tangentTransform, viewDirection).xy + i.uv0);
                float4 _node_9690_var = tex2D(_node_9690,TRANSFORM_TEX(node_855.rg, _node_9690));
                float4 node_7160 = _Time + _TimeEditor;
                float3 emissive = (_node_8063_var.rgb+(_node_7858.rgb*(pow((1.0 - _node_8063_var.b),_node_8597)*_node_9690_var.rgb*(_node_8911.rgb*cos((node_7160.g/_node_397))*_node_397*_node_8575*_node_7858.b))));
/// Final Color:
                float3 finalColor = specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform sampler2D _node_8063; uniform float4 _node_8063_ST;
            uniform float _node_2869;
            uniform sampler2D _node_9690; uniform float4 _node_9690_ST;
            uniform float _node_8597;
            uniform float4 _node_7858;
            uniform float _node_1572;
            uniform float _node_397;
            uniform float4 _node_8911;
            uniform float _node_8575;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 0.5;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float3 specularColor = float3(_node_1572,_node_1572,_node_1572);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/// Final Color:
                float3 finalColor = specular;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Shader Forge/shander_hole"
    CustomEditor "ShaderForgeMaterialInspector"
}

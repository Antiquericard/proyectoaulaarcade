// Shader created with Shader Forge v1.32 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.32;sub:START;pass:START;ps:flbk:Custom/shader_deutrino,iptp:0,cusa:True,bamd:0,lico:1,lgpr:1,limd:1,spmd:0,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:True,rprd:False,enco:True,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4013,x:34603,y:33129,varname:node_4013,prsc:2|diff-855-OUT,diffpow-5997-RGB,spec-8713-OUT,emission-7328-OUT,amspl-7328-OUT,alpha-8713-OUT;n:type:ShaderForge.SFN_TexCoord,id:4666,x:31139,y:32684,varname:node_4666,prsc:2,uv:0;n:type:ShaderForge.SFN_Vector1,id:897,x:31127,y:32567,varname:node_897,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:696,x:31383,y:32635,varname:node_696,prsc:2|A-897-OUT,B-4666-UVOUT;n:type:ShaderForge.SFN_Panner,id:9760,x:31572,y:32635,varname:node_9760,prsc:2,spu:1,spv:1|UVIN-696-OUT;n:type:ShaderForge.SFN_Tex2d,id:6117,x:31826,y:32635,ptovrint:False,ptlb:node_6117,ptin:_node_6117,varname:_node_6117,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False|UVIN-9760-UVOUT,MIP-9339-U;n:type:ShaderForge.SFN_RemapRange,id:859,x:32084,y:32641,varname:node_859,prsc:2,frmn:0,frmx:1,tomn:0,tomx:1|IN-6117-R;n:type:ShaderForge.SFN_Multiply,id:970,x:32314,y:32641,varname:node_970,prsc:2|A-859-OUT,B-5207-OUT,C-9339-V;n:type:ShaderForge.SFN_ValueProperty,id:5207,x:32084,y:32841,ptovrint:False,ptlb:node_5207,ptin:_node_5207,varname:_node_5207,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Add,id:3841,x:32599,y:32637,varname:node_3841,prsc:2|A-970-OUT,B-4527-OUT;n:type:ShaderForge.SFN_TexCoord,id:9339,x:31491,y:32980,varname:node_9339,prsc:2,uv:0;n:type:ShaderForge.SFN_RemapRange,id:3314,x:31772,y:32984,varname:node_3314,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-9339-UVOUT;n:type:ShaderForge.SFN_ComponentMask,id:8203,x:32084,y:32985,varname:node_8203,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-3314-OUT;n:type:ShaderForge.SFN_Length,id:3746,x:31772,y:33196,varname:node_3746,prsc:2|IN-3314-OUT;n:type:ShaderForge.SFN_ArcTan2,id:4527,x:32440,y:32994,varname:node_4527,prsc:2,attp:3|A-8203-G,B-8203-R;n:type:ShaderForge.SFN_Multiply,id:2644,x:32107,y:33202,varname:node_2644,prsc:2|A-3746-OUT,B-9764-OUT;n:type:ShaderForge.SFN_Vector1,id:9764,x:31921,y:33440,varname:node_9764,prsc:2,v1:0.6;n:type:ShaderForge.SFN_Time,id:964,x:31491,y:33497,varname:node_964,prsc:2;n:type:ShaderForge.SFN_Vector1,id:1730,x:31491,y:33712,varname:node_1730,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:3672,x:31773,y:33506,varname:node_3672,prsc:2|A-964-TSL,B-1730-OUT;n:type:ShaderForge.SFN_Add,id:7376,x:32571,y:33198,varname:node_7376,prsc:2|A-2644-OUT,B-3672-OUT;n:type:ShaderForge.SFN_Tex2d,id:5024,x:33096,y:32955,ptovrint:False,ptlb:node_5024,ptin:_node_5024,varname:_node_5024,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:4dd7429a71a7ba4488571a38fc43eeb1,ntxv:0,isnm:False|UVIN-5052-OUT;n:type:ShaderForge.SFN_Append,id:5052,x:32874,y:32955,varname:node_5052,prsc:2|A-7376-OUT,B-3841-OUT;n:type:ShaderForge.SFN_Power,id:5844,x:32253,y:33897,varname:node_5844,prsc:2|VAL-3746-OUT,EXP-1895-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1895,x:32001,y:34070,ptovrint:False,ptlb:node_1895,ptin:_node_1895,varname:_node_1895,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:4;n:type:ShaderForge.SFN_Multiply,id:593,x:32448,y:33990,varname:node_593,prsc:2|A-5844-OUT,B-18-OUT;n:type:ShaderForge.SFN_ValueProperty,id:18,x:32001,y:34239,ptovrint:False,ptlb:node_18,ptin:_node_18,varname:_node_18,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.7;n:type:ShaderForge.SFN_Multiply,id:610,x:32448,y:33805,varname:node_610,prsc:2|A-5024-R,B-5844-OUT;n:type:ShaderForge.SFN_Add,id:4888,x:32737,y:33804,varname:node_4888,prsc:2|A-5024-G,B-610-OUT,C-593-OUT;n:type:ShaderForge.SFN_Multiply,id:2856,x:32970,y:33804,varname:node_2856,prsc:2|A-4888-OUT,B-9190-OUT;n:type:ShaderForge.SFN_Floor,id:9485,x:32448,y:34227,varname:node_9485,prsc:2|IN-5111-OUT;n:type:ShaderForge.SFN_Set,id:7140,x:31921,y:33298,varname:cir,prsc:2|IN-3746-OUT;n:type:ShaderForge.SFN_Get,id:5111,x:32191,y:34345,varname:node_5111,prsc:2|IN-7140-OUT;n:type:ShaderForge.SFN_OneMinus,id:9252,x:32688,y:34227,varname:node_9252,prsc:2|IN-9485-OUT;n:type:ShaderForge.SFN_Vector1,id:1054,x:32179,y:34505,varname:node_1054,prsc:2,v1:2;n:type:ShaderForge.SFN_Get,id:4282,x:32191,y:34426,varname:node_4282,prsc:2|IN-7140-OUT;n:type:ShaderForge.SFN_Multiply,id:2322,x:32459,y:34391,varname:node_2322,prsc:2|A-4282-OUT,B-1054-OUT;n:type:ShaderForge.SFN_Clamp01,id:9190,x:32688,y:34391,varname:node_9190,prsc:2|IN-2322-OUT;n:type:ShaderForge.SFN_OneMinus,id:6307,x:32915,y:34483,varname:node_6307,prsc:2|IN-9190-OUT;n:type:ShaderForge.SFN_Multiply,id:2596,x:33142,y:34392,varname:node_2596,prsc:2|A-9190-OUT,B-6307-OUT;n:type:ShaderForge.SFN_Power,id:5524,x:33233,y:33800,varname:node_5524,prsc:2|VAL-2856-OUT,EXP-5773-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5773,x:32970,y:34001,ptovrint:False,ptlb:node_5773,ptin:_node_5773,varname:_node_5773,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Add,id:4165,x:33460,y:33800,varname:node_4165,prsc:2|A-5524-OUT,B-7-OUT,C-2596-OUT;n:type:ShaderForge.SFN_TexCoord,id:7662,x:31855,y:34639,varname:node_7662,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:6842,x:31855,y:34804,varname:node_6842,prsc:2|A-269-OUT,B-7662-UVOUT;n:type:ShaderForge.SFN_Add,id:4782,x:32081,y:34804,varname:node_4782,prsc:2|A-6842-OUT,B-6722-OUT;n:type:ShaderForge.SFN_ValueProperty,id:269,x:31674,y:35005,ptovrint:False,ptlb:node_269,ptin:_node_269,varname:_node_269,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:3;n:type:ShaderForge.SFN_Multiply,id:3522,x:31864,y:35180,varname:node_3522,prsc:2|A-269-OUT,B-1763-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1763,x:31701,y:35342,ptovrint:False,ptlb:node_1763,ptin:_node_1763,varname:_node_1763,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Subtract,id:1609,x:32059,y:35180,varname:node_1609,prsc:2|A-3522-OUT,B-1763-OUT;n:type:ShaderForge.SFN_Negate,id:6722,x:32289,y:35180,varname:node_6722,prsc:2|IN-1609-OUT;n:type:ShaderForge.SFN_Tex2d,id:4379,x:32377,y:34801,ptovrint:False,ptlb:node_4379,ptin:_node_4379,varname:_node_4379,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:8689fac7742124e4ebd4c379ee337bcc,ntxv:0,isnm:False|UVIN-4782-OUT;n:type:ShaderForge.SFN_ComponentMask,id:2663,x:32610,y:34801,varname:node_2663,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-4379-RGB;n:type:ShaderForge.SFN_RemapRange,id:5947,x:32871,y:34804,varname:node_5947,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-2663-OUT;n:type:ShaderForge.SFN_TexCoord,id:9554,x:32180,y:35464,varname:node_9554,prsc:2,uv:0;n:type:ShaderForge.SFN_Panner,id:1881,x:32509,y:35348,varname:node_1881,prsc:2,spu:1,spv:0|UVIN-9554-UVOUT;n:type:ShaderForge.SFN_Panner,id:6899,x:32509,y:35547,varname:node_6899,prsc:2,spu:0,spv:1|UVIN-9554-UVOUT;n:type:ShaderForge.SFN_Add,id:1714,x:32831,y:35343,varname:node_1714,prsc:2|A-5947-OUT,B-1881-UVOUT,C-9554-V;n:type:ShaderForge.SFN_Add,id:7434,x:32831,y:35551,varname:node_7434,prsc:2|A-5947-OUT,B-6899-UVOUT,C-9554-U;n:type:ShaderForge.SFN_Multiply,id:9780,x:33087,y:35343,varname:node_9780,prsc:2|A-1714-OUT,B-9483-OUT;n:type:ShaderForge.SFN_Multiply,id:9643,x:33087,y:35551,varname:node_9643,prsc:2|A-7434-OUT,B-9483-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9483,x:32867,y:35759,ptovrint:False,ptlb:node_9483,ptin:_node_9483,varname:_node_9483,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Tex2dAsset,id:5675,x:33392,y:35345,ptovrint:True,ptlb:node_5675,ptin:_node_5675,varname:_node_5675,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:3a5a96df060a5cf4a9cc0c59e13486b7,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:57,x:33692,y:35216,varname:_node_57,prsc:2,tex:3a5a96df060a5cf4a9cc0c59e13486b7,ntxv:0,isnm:False|UVIN-9780-OUT,TEX-5675-TEX;n:type:ShaderForge.SFN_Tex2d,id:5189,x:33692,y:35485,varname:_node_5189,prsc:2,tex:3a5a96df060a5cf4a9cc0c59e13486b7,ntxv:0,isnm:False|UVIN-9643-OUT,TEX-5675-TEX;n:type:ShaderForge.SFN_Multiply,id:8106,x:34075,y:35223,varname:node_8106,prsc:2|A-57-R,B-57-R,C-57-R;n:type:ShaderForge.SFN_Multiply,id:8052,x:34073,y:35511,varname:node_8052,prsc:2|A-5189-R,B-5189-G,C-5189-B;n:type:ShaderForge.SFN_Add,id:8408,x:34341,y:35327,varname:node_8408,prsc:2|A-8106-OUT,B-8052-OUT;n:type:ShaderForge.SFN_Clamp01,id:7180,x:34578,y:35327,varname:node_7180,prsc:2|IN-8408-OUT;n:type:ShaderForge.SFN_Multiply,id:7,x:34800,y:35327,varname:node_7,prsc:2|A-7180-OUT,B-4379-B,C-7317-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7317,x:34629,y:35555,ptovrint:False,ptlb:node_7317,ptin:_node_7317,varname:_node_7317,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:10;n:type:ShaderForge.SFN_Multiply,id:6903,x:33704,y:33813,varname:node_6903,prsc:2|A-4165-OUT,B-9252-OUT,C-1598-OUT;n:type:ShaderForge.SFN_Clamp01,id:8713,x:33912,y:33813,varname:node_8713,prsc:2|IN-6903-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1598,x:33661,y:34006,ptovrint:False,ptlb:node_1598,ptin:_node_1598,varname:_node_1598,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Color,id:2940,x:33827,y:33159,ptovrint:False,ptlb:node_2940,ptin:_node_2940,varname:_node_2940,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2292088,c2:0.2077206,c3:0.8308824,c4:1;n:type:ShaderForge.SFN_Color,id:1089,x:33841,y:33420,ptovrint:False,ptlb:node_1089,ptin:_node_1089,varname:_node_1089,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.8235294,c2:0.1513841,c3:0.1652904,c4:1;n:type:ShaderForge.SFN_Lerp,id:7328,x:34115,y:33209,varname:node_7328,prsc:2|A-2940-RGB,B-1089-RGB,T-8713-OUT;n:type:ShaderForge.SFN_Tex2d,id:5997,x:33827,y:32900,ptovrint:False,ptlb:node_5997,ptin:_node_5997,varname:_node_5997,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:51aa11ae9f6e54c76b49ebb6f6bf55a3,ntxv:0,isnm:False|MIP-3841-OUT;n:type:ShaderForge.SFN_Reflect,id:3085,x:33568,y:32757,varname:node_3085,prsc:2|A-5024-RGB,B-5024-B;n:type:ShaderForge.SFN_Multiply,id:855,x:33980,y:32690,varname:node_855,prsc:2|A-3085-OUT,B-5024-R;proporder:6117-5207-5024-1895-18-5773-269-1763-4379-9483-5675-7317-1598-2940-1089-5997;pass:END;sub:END;*/

Shader "Shader Forge/NewShader" {
    Properties {
        _node_6117 ("node_6117", 2D) = "white" {}
        _node_5207 ("node_5207", Float ) = 1
        _node_5024 ("node_5024", 2D) = "white" {}
        _node_1895 ("node_1895", Float ) = 4
        _node_18 ("node_18", Float ) = 0.7
        _node_5773 ("node_5773", Float ) = 2
        _node_269 ("node_269", Float ) = 3
        _node_1763 ("node_1763", Float ) = 0.5
        _node_4379 ("node_4379", 2D) = "white" {}
        _node_9483 ("node_9483", Float ) = 0.5
        _node_5675 ("node_5675", 2D) = "white" {}
        _node_7317 ("node_7317", Float ) = 10
        _node_1598 ("node_1598", Float ) = 1
        _node_2940 ("node_2940", Color) = (0.2292088,0.2077206,0.8308824,1)
        _node_1089 ("node_1089", Color) = (0.8235294,0.1513841,0.1652904,1)
        _node_5997 ("node_5997", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "CanUseSpriteAtlas"="True"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform sampler2D _node_6117; uniform float4 _node_6117_ST;
            uniform float _node_5207;
            uniform sampler2D _node_5024; uniform float4 _node_5024_ST;
            uniform float _node_1895;
            uniform float _node_18;
            uniform float _node_5773;
            uniform float _node_269;
            uniform float _node_1763;
            uniform sampler2D _node_4379; uniform float4 _node_4379_ST;
            uniform float _node_9483;
            uniform sampler2D _node_5675; uniform float4 _node_5675_ST;
            uniform float _node_7317;
            uniform float _node_1598;
            uniform float4 _node_2940;
            uniform float4 _node_1089;
            uniform sampler2D _node_5997; uniform float4 _node_5997_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = 0.5;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float2 node_3314 = (i.uv0*2.0+-1.0);
                float node_3746 = length(node_3314);
                float4 node_964 = _Time + _TimeEditor;
                float4 node_3991 = _Time + _TimeEditor;
                float2 node_9760 = ((1.0*i.uv0)+node_3991.g*float2(1,1));
                float4 _node_6117_var = tex2Dlod(_node_6117,float4(TRANSFORM_TEX(node_9760, _node_6117),0.0,i.uv0.r));
                float2 node_8203 = node_3314.rg;
                float node_3841 = (((_node_6117_var.r*1.0+0.0)*_node_5207*i.uv0.g)+(1-abs(atan2(node_8203.g,node_8203.r)/3.14159265359)));
                float2 node_5052 = float2(((node_3746*0.6)+(node_964.r*0.1)),node_3841);
                float4 _node_5024_var = tex2D(_node_5024,TRANSFORM_TEX(node_5052, _node_5024));
                float node_5844 = pow(node_3746,_node_1895);
                float cir = node_3746;
                float node_9190 = saturate((cir*2.0));
                float2 node_4782 = ((_node_269*i.uv0)+(-1*((_node_269*_node_1763)-_node_1763)));
                float4 _node_4379_var = tex2D(_node_4379,TRANSFORM_TEX(node_4782, _node_4379));
                float2 node_5947 = (_node_4379_var.rgb.rg*2.0+-1.0);
                float2 node_9780 = ((node_5947+(i.uv0+node_3991.g*float2(1,0))+i.uv0.g)*_node_9483);
                float4 _node_57 = tex2D(_node_5675,TRANSFORM_TEX(node_9780, _node_5675));
                float2 node_9643 = ((node_5947+(i.uv0+node_3991.g*float2(0,1))+i.uv0.r)*_node_9483);
                float4 _node_5189 = tex2D(_node_5675,TRANSFORM_TEX(node_9643, _node_5675));
                float node_8713 = saturate(((pow(((_node_5024_var.g+(_node_5024_var.r*node_5844)+(node_5844*_node_18))*node_9190),_node_5773)+(saturate(((_node_57.r*_node_57.r*_node_57.r)+(_node_5189.r*_node_5189.g*_node_5189.b)))*_node_4379_var.b*_node_7317)+(node_9190*(1.0 - node_9190)))*(1.0 - floor(cir))*_node_1598));
                float3 node_7328 = lerp(_node_2940.rgb,_node_1089.rgb,node_8713);
                float3 specularColor = float3(node_8713,node_8713,node_8713);
                float specularMonochrome = max( max(specularColor.r, specularColor.g), specularColor.b);
                float normTerm = (specPow + 8.0 ) / (8.0 * Pi);
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*normTerm*specularColor;
                float3 indirectSpecular = (0 + node_7328)*specularColor;
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float4 _node_5997_var = tex2Dlod(_node_5997,float4(TRANSFORM_TEX(i.uv0, _node_5997),0.0,node_3841));
                float3 directDiffuse = pow(max( 0.0, NdotL), _node_5997_var.rgb) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuseColor = (reflect(_node_5024_var.rgb,_node_5024_var.b)*_node_5024_var.r);
                diffuseColor *= 1-specularMonochrome;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float3 emissive = node_7328;
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,node_8713);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform sampler2D _node_6117; uniform float4 _node_6117_ST;
            uniform float _node_5207;
            uniform sampler2D _node_5024; uniform float4 _node_5024_ST;
            uniform float _node_1895;
            uniform float _node_18;
            uniform float _node_5773;
            uniform float _node_269;
            uniform float _node_1763;
            uniform sampler2D _node_4379; uniform float4 _node_4379_ST;
            uniform float _node_9483;
            uniform sampler2D _node_5675; uniform float4 _node_5675_ST;
            uniform float _node_7317;
            uniform float _node_1598;
            uniform float4 _node_2940;
            uniform float4 _node_1089;
            uniform sampler2D _node_5997; uniform float4 _node_5997_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = 0.5;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float2 node_3314 = (i.uv0*2.0+-1.0);
                float node_3746 = length(node_3314);
                float4 node_964 = _Time + _TimeEditor;
                float4 node_8762 = _Time + _TimeEditor;
                float2 node_9760 = ((1.0*i.uv0)+node_8762.g*float2(1,1));
                float4 _node_6117_var = tex2Dlod(_node_6117,float4(TRANSFORM_TEX(node_9760, _node_6117),0.0,i.uv0.r));
                float2 node_8203 = node_3314.rg;
                float node_3841 = (((_node_6117_var.r*1.0+0.0)*_node_5207*i.uv0.g)+(1-abs(atan2(node_8203.g,node_8203.r)/3.14159265359)));
                float2 node_5052 = float2(((node_3746*0.6)+(node_964.r*0.1)),node_3841);
                float4 _node_5024_var = tex2D(_node_5024,TRANSFORM_TEX(node_5052, _node_5024));
                float node_5844 = pow(node_3746,_node_1895);
                float cir = node_3746;
                float node_9190 = saturate((cir*2.0));
                float2 node_4782 = ((_node_269*i.uv0)+(-1*((_node_269*_node_1763)-_node_1763)));
                float4 _node_4379_var = tex2D(_node_4379,TRANSFORM_TEX(node_4782, _node_4379));
                float2 node_5947 = (_node_4379_var.rgb.rg*2.0+-1.0);
                float2 node_9780 = ((node_5947+(i.uv0+node_8762.g*float2(1,0))+i.uv0.g)*_node_9483);
                float4 _node_57 = tex2D(_node_5675,TRANSFORM_TEX(node_9780, _node_5675));
                float2 node_9643 = ((node_5947+(i.uv0+node_8762.g*float2(0,1))+i.uv0.r)*_node_9483);
                float4 _node_5189 = tex2D(_node_5675,TRANSFORM_TEX(node_9643, _node_5675));
                float node_8713 = saturate(((pow(((_node_5024_var.g+(_node_5024_var.r*node_5844)+(node_5844*_node_18))*node_9190),_node_5773)+(saturate(((_node_57.r*_node_57.r*_node_57.r)+(_node_5189.r*_node_5189.g*_node_5189.b)))*_node_4379_var.b*_node_7317)+(node_9190*(1.0 - node_9190)))*(1.0 - floor(cir))*_node_1598));
                float3 specularColor = float3(node_8713,node_8713,node_8713);
                float specularMonochrome = max( max(specularColor.r, specularColor.g), specularColor.b);
                float normTerm = (specPow + 8.0 ) / (8.0 * Pi);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*normTerm*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float4 _node_5997_var = tex2Dlod(_node_5997,float4(TRANSFORM_TEX(i.uv0, _node_5997),0.0,node_3841));
                float3 directDiffuse = pow(max( 0.0, NdotL), _node_5997_var.rgb) * attenColor;
                float3 diffuseColor = (reflect(_node_5024_var.rgb,_node_5024_var.b)*_node_5024_var.r);
                diffuseColor *= 1-specularMonochrome;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * node_8713,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Custom/shader_deutrino"
    CustomEditor "ShaderForgeMaterialInspector"
}

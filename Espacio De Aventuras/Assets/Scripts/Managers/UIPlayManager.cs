﻿
/* 
 * Resume of this project.
 * Copyright (C) Ricardo Ruiz Anaya & Nicolás Robayo Moreno 2017
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIPlayManager : Singleton<UIPlayManager>{

	#region Setting Attributes

	[Tooltip("PauseMenu.")] [SerializeField] public GameObject pause;

	[Tooltip("Monitor de juego de la escena.")] [SerializeField] GameObject playMonitor;

	[Tooltip("Monitor para los créditos")] GameObject creditsMonitor;

	[Tooltip("Cámara de lanzamiento.")] public GameObject launchCamera;

	[Tooltip("Cámara del piloto.")] GameObject pilotCamera;

	[Tooltip("Cámara del minimapa")] GameObject minimapCamera;

	[Tooltip("Tiempo transcurrido en el movimiento de rotación de la cámara del piloto.")] [SerializeField] float tiempo = .04f;

	[Tooltip("Música de MainMenu")] [SerializeField] AudioClip mainMenuMusic;

	[Tooltip("Música de juego")] [SerializeField] AudioClip playMusic;

	[Tooltip("Animator")] [SerializeField] Animator anim;

	bool onPause = true;
	bool onLevelSelector = false;
	bool onCredits = false;

	public Quaternion _defaultRotationPilotCamera;

	GameObject hud;
	GameObject levelSelector;

	float lerp;

	Quaternion targetMonitorRotation = new Quaternion (0.2240156f, 0.2719971f, -0.06526613f, 0.9335818f);

	Quaternion targetLevelsRotation = new Quaternion (-0.07974235f, -0.9157683f, 0.2270177f, -0.3216715f);

	Quaternion creditsMonitorRotation = new Quaternion(0.1039393f,0.8595236f,-0.1782607f,0.4675885f);

	[Tooltip("Imagen de Aula Arcade")][SerializeField] Sprite aulaArcadeImage;
	[Tooltip("Imagen de créditos")][SerializeField] Sprite creditsImage;


	#endregion

	#region Unity Methods

	//Si pulsamos la tecla Esc, entraremos en el menú de pausa.
	void Update () {
		if (Input.GetButtonDown("Cancel") && SceneManager.GetActiveScene().name != "Main Menu"){
			PauseOrResume ();
		}
	}

	#endregion

	#region Public Methods

	// Método para entrar en el menú de pausa.
	public void PauseOrResume () {
		if (onCredits) {
			pause.SetActive (true);
			onCredits = false;
			creditsMonitor.transform.GetChild (0).GetChild (0).GetComponent<Image> ().sprite = aulaArcadeImage;
			StartCoroutine("ReducirVisionYRotarHaciaControles");
		}
		else if (!onPause || (onPause && onLevelSelector)) {
			pause.SetActive (true);
			onLevelSelector = false;
			// Reducir visión durante 1 segundo y rotar cámara hasta el panel de control.
			StartCoroutine ("ReducirVisionYRotarHaciaControles");
		} else {
			// Rotar camara hacia el monitor y ampliar visión durante 1 segundo.
			anim.SetTrigger("Button1");
			StartCoroutine ("RotarHaciaMonitorYAmpliarVision", targetMonitorRotation);
			// ESPERAR A QUE TERMINE LA COROUTINE PARA CONTINUAR...
		}
	}



	// Método para reiniciar el nivel.
	public void ResetLevel () {
		anim.SetTrigger ("Button3");
		StartCoroutine ("ResetLevelC");

	}

	IEnumerator ResetLevelC(){
		yield return new WaitForSecondsRealtime (1f);
		LoadManager.instance.loadAScene (SceneManager.GetActiveScene ().name);
		CancelInvoke ();
	}

	// Método para volver al menú de inicio.
	public void ToMainMenu () {

		// Lanzar coroutine para rotar hacia el monitor de la derecha y ampliar, una vez se realice esto,
		// activamos la cámara del levelsMenu y desactivamos la de la cámara del piloto.
		onLevelSelector = true;
		anim.SetTrigger ("Button2");
		StartCoroutine ("RotarHaciaMonitorYAmpliarVision", targetLevelsRotation);

		//LoadManager.instance.loadAScene ("Main Menu");
	}

	// Método para pasar al siguiente nivel.
	public void NextLevel (){
		LoadManager.instance.loadAScene ("Level " + (SceneManager.GetActiveScene().buildIndex).ToString ());
	}

	[ContextMenu("Reload")]
	public void ReloadVariables () {
		pause = GameObject.Find ("Pause");
		pilotCamera = GameObject.Find ("CameraPilot");
		anim = GameObject.Find ("Pilot").GetComponent<Animator> ();
		playMonitor = GameObject.Find ("PlayMonitor");
		launchCamera = GameObject.Find ("Main Camera");
		minimapCamera = GameObject.Find ("CameraMiniMap");
		hud = GameObject.Find ("GlobalObjects").transform.Find ("HUD").gameObject;
		levelSelector = GameObject.Find ("GlobalObjects").transform.Find ("LevelSelector").gameObject;
		_defaultRotationPilotCamera = pilotCamera.transform.localRotation;
		creditsMonitor = GameObject.Find ("AulaMonitor");
		launchCamera.SetActive (false);
		minimapCamera.SetActive (false);
	}
	#endregion

	#region Coroutines

	/// <summary>
	/// Coroutine para rotar la cámara del piloto hacia un objetivo
	/// </summary>
	/// <param name="target">Quaternion que tendrá la cámara del piloto al mirar al objetivo.</param>
	IEnumerator RotarHaciaMonitorYAmpliarVision (Quaternion target) {

		yield return new WaitForSecondsRealtime (1f);

		// Mientrar no estemos mirando al target, estaremos rotando hacia él.
		lerp = 0;
		while (Quaternion.Angle (pilotCamera.transform.rotation, target) > 1f) {
			lerp += Time.unscaledDeltaTime;
			pilotCamera.transform.rotation = Quaternion.Lerp (pilotCamera.transform.rotation, target, lerp);
			yield return new WaitForSecondsRealtime (tiempo);
		}

		// Mientras el fieldOfView sea mayor de 30, reduciremos este parámetro.
		Camera pilotCameraComponent = pilotCamera.GetComponent<Camera> ();
		while (pilotCameraComponent.fieldOfView > 30) {
			pilotCameraComponent.fieldOfView -= 2;
			yield return new WaitForSecondsRealtime (tiempo);
		}

		while (target == creditsMonitorRotation && pilotCameraComponent.fieldOfView > 10) {
			pilotCameraComponent.fieldOfView -= 2;
			yield return new WaitForSecondsRealtime (tiempo);
		}

		pause.SetActive (false);

		// Caso Resume:
		// 1. Habilitamos el movimiento de la cámara de juego.
		// 2. Ponemos el timeScale a 1.
		// 3. Habilitamos el modo disparo.
		if (target == targetMonitorRotation) {
			pilotCamera.GetComponent<Camera> ().depth = 0;
			launchCamera.GetComponent<CameraMovement> ().enabled = true;
			Time.timeScale = 1;
			GameManager.instance.firing.enabled = true;
			onPause = false;
			onLevelSelector = false;
			hud.SetActive (true);

			//Activamos las cámaras de juego
			minimapCamera.SetActive (true);
			launchCamera.SetActive (true);
			//Desactivamos la cámara de piloto
			pilotCamera.SetActive (false);
			//Ponemos musica de juego y quitamos la otra
			SoundManager.instance.StopMusic ();
			SoundManager.instance.PlayMusic (playMusic);

		} else if (target == creditsMonitorRotation) {
			onCredits = true;
			creditsMonitor.transform.GetChild (0).GetChild (0).GetComponent<Image> ().sprite = creditsImage;
		} else {
			onLevelSelector = true;
			levelSelector.SetActive (true);
		}
	}


	IEnumerator ReducirVisionYRotarHaciaControles () {
		//Activamos la cámara de piloto
		pilotCamera.SetActive(true);
		//Desactivamos las cámaras de juego
		minimapCamera.SetActive(false);
		launchCamera.SetActive (false);
		//Ponemos musica de mainMenu y quitamos la otra
		SoundManager.instance.StopMusic();
		SoundManager.instance.PlayMusic (mainMenuMusic);


		if (!onPause) {
			hud.SetActive (false);
			launchCamera.GetComponent<CameraMovement> ().enabled = false;
		} else {
			levelSelector.SetActive (false);
		}

		// Cambio de profundidad en la camara del piloto a 1. 
		Camera pilotCameraComponent = pilotCamera.GetComponent<Camera> ();
		pilotCameraComponent.depth = 2;

		// Reducimos la visión de la cámara.
		while (pilotCameraComponent.fieldOfView < 67) {
			pilotCameraComponent.fieldOfView+=2;
			yield return new WaitForSecondsRealtime (tiempo);
		}

		// Rotamos la cámara hacia el panel de control.
		float lerp = 0;
		while (Quaternion.Angle(pilotCamera.transform.localRotation,_defaultRotationPilotCamera) > 1f) {
			lerp += Time.unscaledDeltaTime;
			pilotCamera.transform.localRotation = Quaternion.Slerp (pilotCamera.transform.localRotation, _defaultRotationPilotCamera, lerp);
			yield return new WaitForSecondsRealtime (tiempo);
		}

		if (!onPause) {
			Time.timeScale = 0;
			GameManager.instance.firing.enabled = false;
			onPause = true;
		}
	}

	public void ToCredits(){
		anim.SetTrigger ("Button2");
		StartCoroutine ("RotarHaciaMonitorYAmpliarVision", creditsMonitorRotation);
	}

	#endregion
}
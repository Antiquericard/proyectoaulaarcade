﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class MenuButton : MonoBehaviour {

	[Tooltip("Sonido de boton")] public AudioClip buttonSound;

	public void PlaySound(){
		if (this.GetComponent<Button> ().IsInteractable ()) {
			SoundManager.instance.PlaySound (buttonSound);
		}
	}
}

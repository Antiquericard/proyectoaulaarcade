﻿
/* 
 * Resume of this project.
 * Copyright (C) Ricardo Ruiz Anaya & Nicolás Robayo Moreno 2017
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SeleccionadorNiveles : MonoBehaviour {

	#region Setting Attributes

	[Tooltip("Colocar el botón a repetir según el número de niveles.")] [SerializeField] GameObject boton;

	[Tooltip("Colocar el padre de la posicion en la jerarquía del botón.")] [SerializeField] Transform parent;

	[Tooltip("Total de niveles.")] [SerializeField] private int totalLevels;

	[Tooltip("Array de botones de niveles.")] [SerializeField] GameObject[] buttons;

	[Tooltip("Escenas que no son niveles.")] [SerializeField] byte escenasNoLevel;

	// Niveles completados.

	[SerializeField] int levelsCompleted;

	GameObject but;

	#endregion

	#region Unity Methods

	// Se carga la partida
	void Awake(){
		levelsCompleted = GameManager.instance.save.level;
	}

	void Start () {

		for (int i = 0; i < buttons.Length; i++){
			but = buttons [i];

			//Es 2 porque hay dos escenas que no son niveles
			if (levelsCompleted >= i + 2) {
				//Si es asi, el nivel fue completado
				string scene = "Level " + (i+escenasNoLevel-1).ToString ();
				but.GetComponent<Button> ().onClick.AddListener( () => this.gameObject.SetActive(false));
				but.GetComponent<Button> ().onClick.AddListener( () => LoadManager.instance.loadAScene(scene));
				but.GetComponent<Button>().interactable = true; //En principio no es necesaria esta línea, pero por si acaso..
			} else {
				//Si no, el nivel está bloqueado todavía
				but.GetComponent<Button>().interactable = false;
			}
		}
	}

	#endregion

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager> {

	[SerializeField] Object musicSourcePrefab;
	[SerializeField] Object soundSourcePrefab;
	List<AudioSource> musicSources = new List<AudioSource>();
	List<AudioSource> soundSources = new List<AudioSource>();
	int maxMusicSources = 1;
	int maxSoundSources = 2;

	AudioClip clip;
	AudioSource musicSource;

	void Awake () {
		DontDestroyOnLoad (gameObject);
	}

	public AudioSource GetMusicSource () {
		foreach (AudioSource source in musicSources) {
			if (!source.gameObject.activeSelf) {
				source.gameObject.SetActive(true);
				return source;
			}
		}

		if (musicSources.Count <= maxMusicSources) {
			GameObject source = Instantiate (musicSourcePrefab) as GameObject;
			source.transform.parent = transform;
			AudioSource audio = source.GetComponent<AudioSource> ();
			musicSources.Add (audio);
			return audio;
		} else {
			Debug.LogWarning ("Ha pasado algo feo. No debería pedirse más música");
			//TODO Devolvemos null para que no suene nada
			return null;
		}

	}

	public AudioSource GetSoundSource () {
		foreach (AudioSource source in soundSources) {
			if (!source.gameObject.activeSelf) {
				source.gameObject.SetActive(true);
				return source;
			}
		}

		if (soundSources.Count <= maxSoundSources) {
			GameObject source = Instantiate (soundSourcePrefab) as GameObject;
			source.transform.parent = transform;
			AudioSource audio = source.GetComponent<AudioSource> ();
			soundSources.Add (audio);
			return audio;
		} else {
			Debug.LogWarning ("Ha pasado algo feo. No deberían pedirse más sonidos");
			//TODO Devolvemos null para que no suene nada
			return null;
		}

	}

	public void PlayMusic(AudioClip music){
		//Los clips son en play on awake
		AudioSource source = GetMusicSource ();
		source.clip = music;
		source.loop = true;
		source.Play ();
		musicSource = source;
	}

	public void StopMusic(){
		musicSource.Stop ();
		musicSource.gameObject.SetActive (false);
	}

	public void PlaySound(AudioClip sound){
		clip = sound;
		StartCoroutine ("PlaySoundCoroutine");

	}

	public IEnumerator PlaySoundCoroutine(){
		AudioSource source = GetSoundSource ();
		source.clip = clip;
		source.Play ();
		yield return new WaitForSecondsRealtime (clip.length);
		source.gameObject.SetActive (false);
	}

}

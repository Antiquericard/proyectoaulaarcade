﻿
/* 
 * Resume of this project.
 * Copyright (C) Ricardo Ruiz Anaya 2017
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ReloadCanvas : MonoBehaviour {

	#region Setting Attributes
	
	[Tooltip("Botones Main Menu del ButtonsCanvas.")] [SerializeField] Button[] mainMenus;

	[Tooltip("Botones Reset del ButtonsCanvas.")] [SerializeField] Button[] resets;

	[Tooltip("Botón pause del ButtonsCanvas.")] [SerializeField] Button pause;

	[Tooltip("Botón nextLevel del ButtonsCanvas.")] [SerializeField] Button nextLevel;

	[Tooltip("Botón de créditos")] [SerializeField] Button credits;
	#endregion

	#region Unity Methods

	void Start(){
		Time.timeScale = 0f;
		foreach (Button but in mainMenus) {
			but.onClick.AddListener ( () => UIPlayManager.instance.ToMainMenu() );
		}
		foreach (Button but in resets) {
			but.onClick.AddListener ( () => UIPlayManager.instance.ResetLevel() );
		}
		pause.onClick.AddListener( () => UIPlayManager.instance.PauseOrResume() );
		nextLevel.onClick.AddListener ( () => UIPlayManager.instance.NextLevel() );
		credits.onClick.AddListener (() => UIPlayManager.instance.ToCredits ());
	}

	#endregion

}

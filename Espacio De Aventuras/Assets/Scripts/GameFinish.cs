﻿
/* 
 * Resume of this project.
 * Copyright (C) Ricardo Ruiz Anaya 2017
 */

using UnityEngine;
using System.Collections;

public class GameFinish : MonoBehaviour {

	void OnTriggerEnter () {
		Time.timeScale = 0.1f;
		LoadManager.instance.loadAScene ("Main Menu");
	}

}

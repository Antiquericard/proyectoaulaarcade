﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour {

	//En el cargador decidimos qué escena cargaremos
	void Start () {
		GameData save = GameManager.instance.save;
		if (save.lastLevelPlayed < save.level) {
			SceneManager.LoadScene ("Main Menu");
		} else {
			//LoadManager.instance.loadAScene (SceneManager.GetSceneByBuildIndex(save.lastLevelPlayed).name);
			SceneManager.LoadScene(save.lastLevelPlayed);
		}

	}
}
